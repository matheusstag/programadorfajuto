﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ProgramadorFajuto.Aplicacao.Modelos
{
    public class ModeloDeFormatadorDeEmailMarketing
    {
        public string Color { get; set; }
        public string BackgroundColor { get; set; }
        public string FontFamily { get; set; }
        public string FontWeight { get; set; }
        public IFormFile File { get; set; }
    }
}
