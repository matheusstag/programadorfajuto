﻿using ProgramadorFajuto.Aplicacao.Modelos;
using System.IO;

namespace ProgramadorFajuto.Aplicacao.Servicos
{
    public interface IServicoDeFormatacaoDeEmailMarketing
    {
        Stream FormatarEmailMarketing(ModeloDeFormatadorDeEmailMarketing modelo);
    }
}
