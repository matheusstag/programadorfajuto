﻿using Microsoft.AspNetCore.Mvc;

namespace ProgramadorFajuto.View.CustomExtensions
{
    public static class ControllerExtensions
    {
        public static void AdicionarMensagemDeSucesso(this Controller controller, string mensagem)
        {
            controller.TempData["MensagemDeSucesso"] = mensagem;
        }

        public static void AdicionarMensagemDeErro(this Controller controller, string mensagem)
        {
            controller.TempData["MensagemDeErro"] = mensagem;
        }
    }
}
