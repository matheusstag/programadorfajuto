﻿(function (ProgramadorFajutoGeral, $) {
    ProgramadorFajutoGeral.AdicionarMensagemDeErro = function (mensagem) {
        toastr.error(mensagem);
    }

    ProgramadorFajutoGeral.AdicionarMensagemDeSucesso = function (mensagem) {
        toastr.success(mensagem);
    }
})(window.ProgramadorFajutoGeral = window.ProgramadorFajutoGeral || {}, jQuery)