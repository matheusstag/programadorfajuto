﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ProgramadorFajuto.Aplicacao.Modelos;
using ProgramadorFajuto.Aplicacao.Servicos;
using ProgramadorFajuto.View.CustomExtensions;
using System;

namespace ProgramadorFajuto.View.Controllers
{
    public class FerramentasController : Controller
    {
        private IHostingEnvironment _environment;
        private IServicoDeFormatacaoDeEmailMarketing _servicoDeFormatacaoDeEmailMarketing;

        public FerramentasController(IHostingEnvironment environment, IServicoDeFormatacaoDeEmailMarketing servicoDeFormatacaoDeEmailMarketing)
        {
            this._environment = environment;
            this._servicoDeFormatacaoDeEmailMarketing = servicoDeFormatacaoDeEmailMarketing;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult FormatadorDeEmailMarketing()
        {
            return View();
        }

        [HttpPost]
        public IActionResult FormatadorDeEmailMarketing(ModeloDeFormatadorDeEmailMarketing modelo)
        {
            if (modelo.BackgroundColor == null || modelo.Color == null || modelo.File == null || modelo.FontFamily == null || modelo.FontWeight == null)
            {
                this.AdicionarMensagemDeErro("Algum dado do formulário está nulo. Você está brincando com minha cara?");
                return View();
            }

            return File(this._servicoDeFormatacaoDeEmailMarketing.FormatarEmailMarketing(modelo), "application/file", "EmailFormatado.html");
        }
    }
}
