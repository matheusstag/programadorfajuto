﻿using System;
using System.IO;
using System.Text;
using ProgramadorFajuto.Infraestrutura.Extensions;
using ProgramadorFajuto.Aplicacao.Modelos;
using ProgramadorFajuto.Aplicacao.Servicos;

namespace ProgramadorFajuto.Infraestrutura.ServicoDeFormatacaoDeEmailMarketing
{
    public class ServicoDeFormatacaoDeEmailMarketing : IServicoDeFormatacaoDeEmailMarketing
    {
        public Stream FormatarEmailMarketing(ModeloDeFormatadorDeEmailMarketing modelo)
        {
            var texto = "";

            using (var streamReader = new StreamReader(modelo.File.OpenReadStream()))
            {
                var conteudo = streamReader.ReadToEnd();
                texto = conteudo;
            }

            var textinho = new StringBuilder();
            var textoTratado = new StringBuilder(texto);

            if (textoTratado.ToString().Contains("</td>"))
                textoTratado = textoTratado.Replace("</td>", "\r\t\t</td>");

            if (textoTratado.ToString().Contains("alt=\"\""))
                textoTratado = textoTratado.Replace("alt=\"\"", "align=\"top\" alt=\"\"");

            var linhasDoTexto = textoTratado.ToString().Split(new[] { '\r' });

            linhasDoTexto = linhasDoTexto.RemoveAt(6);
            linhasDoTexto = linhasDoTexto.RemoveAt(linhasDoTexto.Length - 4);

            for (int i = 0; i < linhasDoTexto.Length; i++)
            {
                if (linhasDoTexto[i].Contains("<title>"))
                {
                    linhasDoTexto[i] = "<title></title>";
                }

                if (linhasDoTexto[i].Contains("<img"))
                {
                    var alturaDaTagImg = linhasDoTexto[i].Substring(linhasDoTexto[i].IndexOf("width="), linhasDoTexto[i].IndexOf(" alt=\"\"") - linhasDoTexto[i].IndexOf("width="));
                    linhasDoTexto[i - 1] = linhasDoTexto[i - 1].Replace(">", " " + alturaDaTagImg + ">");
                    linhasDoTexto[i - 1] = linhasDoTexto[i - 1].Replace("align", "valign");
                    linhasDoTexto[i] = linhasDoTexto[i].Replace("alt=\"\"", "alt=\"\" style=\"display:block;\"");
                }

                if (linhasDoTexto[i].Contains("texto"))
                {
                    linhasDoTexto[i - 1] = linhasDoTexto[i - 1].Replace("valign=\"top\"", "valign=\"top\" style=\"background-color:" + modelo.BackgroundColor + ";\"");

                    var largura = linhasDoTexto[i].Substring(linhasDoTexto[i].IndexOf("width=") + 7, (linhasDoTexto[i].IndexOf("height=") - 2) - (linhasDoTexto[i].IndexOf("width=") + 7));
                    var altura = linhasDoTexto[i].Substring(linhasDoTexto[i].IndexOf("height=") + 8, (linhasDoTexto[i].IndexOf("align=") - 2) - (linhasDoTexto[i].IndexOf("height=") + 8));
                    var estaLinha = linhasDoTexto[i].Substring(linhasDoTexto[i].IndexOf("<"));
                    var lineHeight = linhasDoTexto[i].Substring(linhasDoTexto[i].IndexOf("texto-") + 6, (linhasDoTexto[i].IndexOf(">")) - (linhasDoTexto[i].IndexOf("texto-") + 6));

                    linhasDoTexto[i] = linhasDoTexto[i].Replace(estaLinha, "<div style=\"width:" + largura + "px;height:" + altura + "px;color:" + "fff" + ";background-color:" + modelo.BackgroundColor + ";text-align:center;\">\r\t\t\t\t" +
                        "<font style=\"width:" + largura + "px;height:" + altura + "px;line-height:" + (int.Parse(altura)) / int.Parse(lineHeight) + "px;color:" + modelo.Color + ";background-color:" + modelo.BackgroundColor + ";text-align:center;font-family:" + modelo.FontFamily +
                        ";font-size:" + (((int.Parse(altura)) / int.Parse(lineHeight)) - 4) + "px;font-weight:" + modelo.FontWeight + ";\">\r" + "\t\t\t\t\tOlá, #participante#" + "\r\t\t\t\t</font>\r\t\t\t</div>");
                }
            }

            for (int i = 0; i < linhasDoTexto.Length; i++)
            {
                textinho.AppendLine(linhasDoTexto[i]);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(textinho.ToString()));
        }
    }
}
